# Ansible script for install HEE

https://www.ibm.com/support/knowledgecenter/en/SSHGWL_2.0.0/local/hadoop.html#hadoop__install-config


- Change `inventory.yml` file to set `HOSTS` and `VARIABLES`
- `wshi-2.0.2-noarch.rpm` is empty - placeholder for true installation file
- Change `install_hee.yml` for users / location / permissions.
- For the Licence, accepted values are `A` for Accept, and `R` for Reject


---

example `ansible` command
```
ansible-playbook -i inventory.yml -l test -u root -k install_hee.yml
```